package com.example.sampleimage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;

public class BitmapCombinePunch {

	private static final String TAG = BitmapCombinePunch.class.getSimpleName();

	public static Bitmap combine(Bitmap b1, Bitmap b2) {
		try {
			int maxWidth = b1.getWidth();
			int maxHeight = b1.getHeight();
			Bitmap bmOverlay = Bitmap.createBitmap(maxWidth, maxHeight, b1.getConfig());
			Canvas canvas = new Canvas(bmOverlay);
			canvas.drawBitmap(b2, 0, 0, null);
			canvas.drawBitmap(b1, 0, 0, null);
			for (int index = 0; index < bmOverlay.getWidth(); index++) {
				for (int index2 = 0; index2 < bmOverlay.getHeight(); index2++) {
					if (bmOverlay.getPixel(index, index2) == Color.rgb(255, 255, 255)) {
						bmOverlay.setPixel(index, index2, Color.TRANSPARENT);
					}
				}
			}
			return bmOverlay;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
