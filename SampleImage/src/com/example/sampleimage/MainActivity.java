package com.example.sampleimage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();

	private ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		imageView = (ImageView) findViewById(R.id.image);

		Bitmap border = BitmapFactory.decodeResource(getResources(), R.drawable.profile_pic_border_alt);
		Bitmap panggap = BitmapFactory.decodeResource(getResources(), R.drawable.panggap);
		panggap = ThumbnailUtils.extractThumbnail(panggap, border.getWidth(), border.getHeight());

		Bitmap b = BitmapCombinePunch.combine(border, panggap);
		imageView.setImageBitmap(b);
	}
}
